(define-module (machines)
  #:use-module (machines kresd)
  #:use-module (machines nginx)
  #:use-module (my-vm system)
  #:export (%kresd-configuration
             %nginx-configuration))

(define %kresd-configuration
  (kresd-vm
   100
   "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA6cCqhVPJi+GKpJ0QsbvM8n5nRdXWHNPrLPlhGK+zXQ"))

(define %nginx-configuration
  (nginx-vm
   111
   "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJHIaG4W4JVg6pvQvaqdSMdNXfL9Qm63jBtMIUhCzxqM"))

(define %nanein-vms
  (map
    vm-configuration->system
    (list %kresd-configuration
          %nginx-configuration)))

(define %other-machines '())

(append
  %nanein-vms
  %other-machines)

; vim: set nospell :
