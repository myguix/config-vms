(define-module (machines kresd)
               #:use-module (guix gexp)
               #:use-module (gnu services)
               #:use-module (gnu services admin)
               #:use-module (gnu services dns)
               #:use-module (my-vm system records)
               #:use-module (my-vm system)
               #:export (kresd-vm))

(define (kresd-vm vmid ssh-key)
  (vm-configuration
    (id vmid)
    (name "kresd")
    (key ssh-key)
    (services
      (list
        (simple-service 'rotate-nginx
                        rottlog-service-type
                        (list
                          (log-rotation
                            (files '("/var/log/nginx/*"))
                            (options '("rotate 6"
                                       "notifempty"
                                       "delaycompress")))))
        (service log-cleanup-service-type
                 (log-cleanup-configuration
                   (directory "/var/log/nginx")))

        ;; The definition can be found in the Guix manual.
        (service
          knot-resolver-service-type
          (knot-resolver-configuration
            (kresd-config-file
              (plain-file "kresd.conf"
"-- -*- mode: lua -*-
net = { '0.0.0.0', '::1' }
user('knot-resolver', 'knot-resolver')
modules = { 'hints > iterate', 'stats', 'predict' }
cache.size = 100 * MB
"))))))
                          (udp-ports '(53))
                          (tcp-ports '(22 953))))
