(define-module (machines nginx)
               #:use-module (machines nginx-conf)
               #:use-module (guix gexp)
               #:use-module (gnu services)
               #:use-module (gnu services admin)
               #:use-module (gnu packages tls)
               #:use-module (gnu services web)
               #:use-module (gnu packages version-control)
               #:use-module (gnu packages certs)
               #:use-module (gnu packages dns)
               #:use-module (gnu services mcron)
               #:use-module (gnu packages golang)
               #:use-module (my-packages web)
               #:use-module (my-vm services nginx)
               #:use-module (my-vm system records)
               #:use-module (my-vm system)
               #:export (nginx-vm))

(use-modules (guix derivations)
             (guix packages)
             (guix store)
             (gnu packages guile)
             (guix gexp)
             (ice-9 i18n)
             (my-packages web))

(define %my-cron-certbot
  #~(job "5 0 * * *"            ;Vixie cron syntax
         "certbot renew"
         "certbot"))

(define (nginx-vm vmid ssh-key)
  (vm-configuration
    (id vmid)
    (name "nginx")
    (key ssh-key)
    (packages (list
                static-nanein-website
                certbot (list knot "tools") go git nss-certs))
    (services
      (list
        (simple-service 'rotate-nginx
                        rottlog-service-type
                        (list
                          (log-rotation
                            (files '("/var/log/nginx/*"))
                            (options '("rotate 6"
                                       "notifempty"
                                       "delaycompress")))))
        (service log-cleanup-service-type
                 (log-cleanup-configuration
                   (directory "/var/log/nginx")))
        (simple-service 'my-cron-jobs
                        mcron-service-type
                        (list %my-cron-certbot))
        (service
          nginx-service-type
          (nginx-configuration (server-blocks (my-server-blocks))))))
    (udp-ports '())
    (tcp-ports '(22 80 443))))
